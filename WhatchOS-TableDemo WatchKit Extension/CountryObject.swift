//
//  CountryObject.swift
//  WhatchOS-TableDemo WatchKit Extension
//
//  Created by MacStudent on 2019-02-27.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit

class CountryObject: NSObject {
    // MARK: class properties
    var name:String?        // ! = this varaible will have a value
    var image:String?       // ? = this variable COULD be null
    
    // MARK: contructor
    convenience override init() {
        self.init(name:"Canada", image:"002-canada")
    }
    
    init(name:String, image:String) {
        self.name = name
        self.image = image
    }

}
