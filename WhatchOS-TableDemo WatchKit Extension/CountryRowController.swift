//
//  CountryRowController.swift
//  WhatchOS-TableDemo WatchKit Extension
//
//  Created by MacStudent on 2019-02-27.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit

class CountryRowController: NSObject {

    @IBOutlet weak var countryNameLabel: WKInterfaceLabel!
    @IBOutlet weak var countryFlag: WKInterfaceImage!
}
