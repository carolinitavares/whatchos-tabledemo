//
//  InterfaceController.swift
//  WhatchOS-TableDemo WatchKit Extension
//
//  Created by MacStudent on 2019-02-27.
//  Copyright © 2019 MacStudent. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    
    // MARK : outlets
    @IBOutlet weak var countryTable: WKInterfaceTable!
    
    // MARK: DataSource
    //let countriesList:[String] = ["Canada", "USA", "Brazil", "India", "Vietnam"]
    //let imageList:[String] = ["001-brazil", "002-canada", "003-india", "004-united-states", "005-vietnam"]
    var countriesList:[CountryObject] = []
    
    func createData() {
        let c1 = CountryObject(name: "Brazil", image: "001-brazil")
        let c2 = CountryObject(name: "Canada", image: "002-canada")
        let c3 = CountryObject(name: "India", image: "003-india")
        let c4 = CountryObject(name: "USA", image: "004-united-states")
        let c5 = CountryObject(name: "Vietnam", image: "005-vietnam")
        
        countriesList.append(c1)
        countriesList.append(c2)
        countriesList.append(c3)
        countriesList.append(c4)
        countriesList.append(c5)
    }


    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("hello")
        
        // 1. Creat your data source
        self.createData()

        // MARK: Populate your tableview with data
        // ------------------
        // 0. tell IOS how many rows your table should have
        self.countryTable.setNumberOfRows(self.countriesList.count, withRowType:"myRow")
        
        // 1. loop through your array
        // 2. take each item in the array and put it in a table row
        for (i, country) in self.countriesList.enumerated() {
            let row = self.countryTable.rowController(at: i) as! CountryRowController
            
            // add name to the row
            row.countryNameLabel.setText(country.name!)
            
            // add the image
            row.countryFlag.setImage(UIImage(named:country.image!))
            
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
